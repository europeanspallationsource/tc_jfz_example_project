﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.16">
  <POU Name="FB_Motor_Persist_Komm" Id="{18271e59-7212-41c1-be55-3018ae3f77fe}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_Motor_Persist_Komm
VAR_INPUT
END_VAR
VAR_OUTPUT
END_VAR

VAR_IN_OUT
	
persist					: ST_MotorPersist; (* persistent data *)
stIOAxisKommunikation 	: ST_AxisKommunikation;

END_VAR

VAR
// Rising edge
fbRTrig_StartAbsMov 	: R_TRIG;
//bQRTrig_StartAbsMov 	: BOOL;
fbRTrig_StartRef 		: R_TRIG;
//bQRTrig_StartRef 		: BOOL;
fbRTrig_Stop 			: R_TRIG;
//bQRTrig_Stop 			: BOOL;
fbRTrig_Reset 			: R_TRIG;
//bQRTrig_Reset 			: BOOL;

fbFTrig_Start_NC		: F_TRIG; 
fbFTrig_Stop_NC			: F_TRIG; 
fbFTrig_Reset_NC		: F_TRIG; 

fbFTrig_StartAbsMov		: F_TRIG; 
fbFTrig_StartRef		: F_TRIG; 
fbFTrig_Stop			: F_TRIG; 
fbFTrig_Reset  			: F_TRIG; 

Error_Code				: BYTE;

END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[// FB_Motor_Persist_Kommunikation
// Baustein zur Übergabe der Daten von der PLC AxisKommunikationsstruktur
// zur Persist Axchenstrucktur
//
// F.Suxdorf  JCNS-1
//
// ## Version 0.1 ##
//


// Persist --> Motor
	
	// Absolutfahrt starten
fbRTrig_StartAbsMov(CLK:= persist.start_abs_mov); // Steigende Falnke auswerten	
IF fbRTrig_StartAbsMov.Q THEN
	stIOAxisKommunikation.Steuerung.rVelocity := persist.speed;
	stIOAxisKommunikation.Steuerung.rPosition := persist.target_pos;
	stIOAxisKommunikation.Steuerung.bAbsolut := TRUE;
	stIOAxisKommunikation.Steuerung.bStart	 := TRUE;	
END_IF

	// Referenzfahrt starten
fbRTrig_StartRef(CLK:= persist.start_ref); // Steigende Falnke auswerten
IF fbRTrig_StartRef.Q THEN
	stIOAxisKommunikation.Steuerung.rVelocity := persist.speed;
	stIOAxisKommunikation.Steuerung.bHoming	 := TRUE;
	stIOAxisKommunikation.Steuerung.bStart	 := TRUE;	
END_IF

	// Stopp
fbRTrig_Stop(CLK:= persist.stop); // Steigende Falnke auswerten
IF fbRTrig_Stop.Q THEN
	stIOAxisKommunikation.Steuerung.bHalt	 := TRUE;	
END_IF

	// Reset
fbRTrig_Reset(CLK:= persist.reset); // Steigende Falnke auswerten
IF fbRTrig_Reset.Q THEN
	stIOAxisKommunikation.Steuerung.bReset	 := TRUE;	
END_IF

// ---------------------------------------------------------------------------------------------------
// Motor --> Persist

// ErrorCode
	      // BIT 0: STOP AT LIMIT Switch
		  // BIT 1: Timeout
		  // Bit 2: Invalid Operation or Parameter
		  // Bit 3: Inhibit/Disable (e.g. ressource like pressed air not available)
		  // Bit 4: Other Error

Error_Code := 2#0000_0000;

IF NOT stIOAxisKommunikation.Rueckmeldung.bLimitFwd OR  NOT stIOAxisKommunikation.Rueckmeldung.bLimitBwd THEN // Limit Switch
		Error_Code	:=	2#0000_0001;
END_IF

IF stIOAxisKommunikation.Rueckmeldung.bError THEN
	Error_Code	:=	2#0000_0100 OR Error_Code;
	IF stIOAxisKommunikation.Error.bBrake_Ack_Error THEN
		Error_Code	:=	2#0000_1000 OR Error_Code;
	END_IF
	IF	stIOAxisKommunikation.Error.bStepper_Module_Error THEN
		Error_Code	:=	2#0001_0000 OR Error_Code;
	END_IF	
END_IF		

persist.error_code 	:= Error_Code; 
persist.actual_pos 	:= lreal_to_real(stIOAxisKommunikation.Rueckmeldung.lrActPosition);
persist.moving 		:= stIOAxisKommunikation.Rueckmeldung.bMoving;
persist.limit_neg	:= stIOAxisKommunikation.Rueckmeldung.bLimitBwd;
persist.limit_pos	:= stIOAxisKommunikation.Rueckmeldung.bLimitfwd;

// DoneBit generieren
fbFTrig_Start_NC(CLK:= stIOAxisKommunikation.Steuerung.bStart);
fbFTrig_Stop_NC(CLK:= stIOAxisKommunikation.Steuerung.bhalt);
fbFTrig_Reset_NC(CLK:= stIOAxisKommunikation.Steuerung.bReset);
IF fbFTrig_Start_NC.Q AND persist.start_abs_mov THEN
	persist.abs_mov_done := TRUE;
ELSIF fbFTrig_Start_NC.Q AND persist.start_ref THEN	
	persist.ref_done := TRUE;
ELSIF fbFTrig_Stop_NC.Q AND persist.stop THEN	
	persist.stop_done := TRUE;
ELSIF fbFTrig_Reset_NC.Q AND persist.reset THEN	
	persist.reset_done := TRUE;	
END_IF

fbFTrig_StartAbsMov(CLK:= persist.start_abs_mov);
fbFTrig_StartRef(CLK:= persist.start_ref);
fbFTrig_Stop(CLK:= persist.stop);
fbFTrig_Reset(CLK:= persist.reset);
IF fbFTrig_StartAbsMov.Q THEN
	persist.abs_mov_done := FALSE;
ELSIF fbFTrig_StartRef.Q THEN	
	persist.ref_done := FALSE;
ELSIF fbFTrig_Stop.Q THEN	
	persist.stop_done := FALSE;
ELSIF fbFTrig_Reset.Q THEN	
	persist.reset_done := FALSE;	
END_IF

]]></ST>
    </Implementation>
    <LineIds Name="FB_Motor_Persist_Komm">
      <LineId Id="479" Count="84" />
      <LineId Id="34" Count="0" />
      <LineId Id="569" Count="1" />
      <LineId Id="581" Count="0" />
      <LineId Id="571" Count="9" />
      <LineId Id="568" Count="0" />
      <LineId Id="619" Count="0" />
      <LineId Id="618" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>